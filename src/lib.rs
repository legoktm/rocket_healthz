/*
Copyright 2020-2021, 2023 Kunal Mehta <legoktm@debian.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
//! The `rocket_healthz` crate provides a fairing to easily add
//! a `/healthz` endpoint to your [Rocket](https://rocket.rs/)
//! project. The endpoint responds with HTTP 200 and the plain
//! text `OK`. You can use it for health checks when
//! you want to verify that the server is running.
//!
//! ```rust
//! #[macro_use] extern crate rocket;
//! use rocket_healthz::Healthz;
//!
//! #[get("/")]
//! fn hello() -> &'static str {
//!     "Hello, world!"
//! }
//!
//! #[launch]
//! fn rocket() -> _ {
//!     rocket::build()
//!         .mount("/", routes![hello])
//!         // vvv - this is the key part, attaching our fairing
//!         .attach(Healthz::fairing())
//! }
//! ```
//!
//! ## Why?
//! This is just syntatic sugar for manually writing and mounting
//! a `/healthz` route, which is already a trivial function:
//! ```
//! #[macro_use] extern crate rocket;
//!
//! #[get("/healthz")]
//! fn healthz() -> &'static str {
//!     "OK"
//! }
//! ```

use rocket::fairing::{Fairing, Info, Kind};
use rocket::{get, routes, Build, Rocket};

#[get("/healthz")]
fn healthz() -> &'static str {
    "OK"
}

/// `/healthz` fairing
pub struct Healthz {}

impl Healthz {
    /// Create the fairing with the default message: `OK`
    pub fn fairing() -> impl Fairing {
        Self {}
    }
}

#[rocket::async_trait]
impl Fairing for Healthz {
    fn info(&self) -> Info {
        Info {
            name: "healthz route",
            kind: Kind::Ignite,
        }
    }

    async fn on_ignite(
        &self,
        rocket: Rocket<Build>,
    ) -> rocket::fairing::Result {
        Ok(rocket.mount("/", routes![healthz]))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rocket::http::Status;
    use rocket::local::blocking::Client;

    // Basic fairing attachment works
    #[test]
    fn test_basic() {
        let rocket = rocket::build().attach(Healthz::fairing());
        let client = Client::tracked(rocket).unwrap();
        let response = client.get("/healthz").dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.into_string(), Some("OK".to_string()));
    }

    // When a conflicting /healthz route is mounted, an error is emitted
    #[test]
    fn test_conflicting_route() {
        #[get("/healthz")]
        fn other_healthz() -> (Status, &'static str) {
            (Status::InternalServerError, "Not OK")
        }

        let rocket = rocket::build()
            .mount("/", rocket::routes![other_healthz])
            .attach(Healthz::fairing());
        let err = Client::tracked(rocket).unwrap_err();
        assert_eq!("collisions detected", err.to_string());
    }
}
