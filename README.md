rocket_healthz
============
[![crates.io](https://img.shields.io/crates/v/rocket_healthz.svg)](https://crates.io/crates/rocket_healthz)
[![docs.rs](https://docs.rs/rocket_healthz/badge.svg)](https://docs.rs/rocket_healthz)
[![pipeline status](https://gitlab.com/legoktm/rocket_healthz/badges/master/pipeline.svg)](https://gitlab.com/legoktm/rocket_healthz/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/rocket_healthz/badges/master/coverage.svg)](https://legoktm.gitlab.io/rocket_healthz/coverage/)

The `rocket_healthz` crate provides a fairing to easily add a `/healthz`
endpoint to your [Rocket](https://rocket.rs/) project.

## License
rocket_healthz is (C) 2020-2021, 2023 Kunal Mehta, released under the Apache 2.0 license,
see LICENSE for details.
